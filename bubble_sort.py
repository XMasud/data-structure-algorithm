def bubble_sort(nums, n):
    for k in range(1, n - 1):
        for i in range(1, n-k):
            if nums[i] > nums[i+1]:
                nums[i], nums[i+1] = nums[i+1], nums[i]

if __name__ == "__main__":
    num_list = [10, 5, 2, 8, 7]
    size = len(num_list)
    bubble_sort(num_list, size)
    print(num_list)
