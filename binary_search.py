def binary_search(low, high, number_arr, num):
    if low <= high:
        mid = (low + high) // 2
        if num == number_arr[mid]:
            return mid
        if number > number_arr[mid]:
            return binary_search(mid + 1, high, number_arr, num)
        else:
            return binary_search(low, mid - 1, number_arr, num)
    else:
        return -1
if __name__ == '__main__':
    number = int(input("Enter searched number: "))
    number_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    low = 0
    high = len(number_list) - 1
    return_index = binary_search(low, high, number_list, number)
    print(return_index)
