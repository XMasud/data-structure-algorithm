def is_prime(num):
    for x in range(2, num//2):
        if num % x == 0:
            return 'Not Prime Number'
    return 'Prime Number'
if __name__ == '__main__':
    number = int(input('Enter number: '))
    result = is_prime(number)
    print(result)