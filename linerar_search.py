def linear_search(number, number_list):
    index = -1
    for key, value in enumerate(number_list):
        if value == number:
            index = key
            break
    return index


if __name__ == '__main__':
    searched_number = int(input("Enter number here: "))
    number_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11]
    res = linear_search(searched_number, number_list)
    print(res)
