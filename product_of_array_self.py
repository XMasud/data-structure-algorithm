def product_of_arrays(lists):
    result_list = []
    for x in range(0, len(lists)):
        value = 1
        for y in range(0, len(lists)):
            if y != x:
                value = value * lists[y]
        result_list.append(value)
    return result_list
if __name__ == '__main__':
    numbers = [0, 0, 3, 4]
    results = product_of_arrays(numbers)
    print(results)